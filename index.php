<!doctype html>

<html lang="fr">

<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="Génerateur de signature Colibris">

	<meta name="author" content="Romain ODET - repatouillé par les gros doigts de Jérémy">

	<meta name="generator" content="PHPSTORM">

	<title>Generateur de signatures Colibris</title>

	<link rel="stylesheet" href="bootstrap.min.css">
	
	<meta name="application-name" content="Mouvement Colibris / prendre sa part, ensemble, plus que jamais."/>

	<meta name="msapplication-TileColor" content="#FFFFFF"/>



	<style>

		.bd-placeholder-img {

			font-size: 1.125rem;

			text-anchor: middle;

			-webkit-user-select: none;

			-moz-user-select: none;

			-ms-user-select: none;

			user-select: none;

		}


		@media (min-width: 768px) {

			.bd-placeholder-img-lg {

				font-size: 3.5rem;

			}

		}

	</style>

	<!-- Custom styles for this template -->

	<link href="cover.css" rel="stylesheet">

	<script>

        function copyToClipboard(element) {

            var $temp = $("<input>");

            $("body").append($temp);

            $temp.val($(element).text()).select();

            document.execCommand("copy");

            $temp.remove();

			displayBlock('collersignature');

        }


        function displayBlock(element) {

            if (document.getElementById(element).style.display == "none") {

                document.getElementById(element).style.display = "block";

            } else {

                document.getElementById(element).style.display = "none";

            }


        }

	</script>

</head>

<body class="text-center">

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

	<header class="masthead mb-auto">

		<div class="inner">

			<h3 class="masthead-brand">Signatures Colibris</h3>

			<nav class="nav nav-masthead justify-content-center">

				<!-- <a class="nav-link active" href="#">Accueil</a> -->

			</nav>

		</div>

	</header>

	<main role="main" class="inner cover" style="margin-top: 1em;">


	
		<!-- The button used to copy the text -->
		
		<?php


			$error = "";

			if (isset($_POST["submit"])) {

				echo('<br><br>

		<button onclick="copyToClipboard(\'#masignature\')" class="btn btn-success">Copier ma signature</button>

		<button onclick="displayBlock(\'masignature\')" class="btn btn-info">Voir le code</button>

		<br><div style="display:none" id="collersignature">Vous pouvez maintenant coller la signature dans votre logiciel de mail</div>

		<br><br><br>');

				$prenom = ucfirst($_POST["prenom"]);

				$nom = strtoupper($_POST["nom"]);

				$email = $_POST["email"];

				$role = $_POST["formrole"];

				$banner = $_POST["url_img_banner"];

				$url_banner = $_POST["url_target_banner"];

				if (empty($_POST["tel-bureau"]) == false) {

					$telbureau = chunk_split($_POST["tel-bureau"], 2, ' ');

				} else {

					$telbureau = "";

				}

				if (empty($_POST["tel-mobile"]) == false) {

					$telmobile = chunk_split($_POST["tel-mobile"], 2, ' ');

				} else {

					$telmobile = "";

				}


				if ($telbureau == "" || $telbureau == " " || $telbureau == '	') {

					$tel = $telmobile;

				} elseif ($telmobile == "" || $telmobile == " " || $telmobile == '	') {

					$tel = $telbureau;

				} elseif ($telmobile == "" || $telmobile == " " && $telbureau == "" || $telbureau == " ") {

					$tel = "";


				} else {

					$tel = $telbureau . '<br>' . $telmobile;

				}
				

				if ($banner == "" && $url_banner == "") {

					$info_banner = '';
		
				} elseif ($banner <> "" && $url_banner == "")  {
		
					$info_banner = '
					<tr>
						<td align="left" colspan="2">
							<img src="'. $banner .'">
						</td>';
		
				} elseif ($banner <> "" && $url_banner <> "")  {
		
					$info_banner = '
					<tr>
						<td align="left" colspan="2">
							<a style="text-decoration:none;" href="'. $url_banner .'" target="_blank"><img src="'. $banner .'" width="440px"></a>
						</td>';
		
		
				} else {
		
					$info_banner = '';
		
				}

				
				$signature_html =

					'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title>Email Signature</title>
	<meta content="text/html; charset=utf-8"http-equiv="Content-Type">
</head>
<body>
<div style="padding:0;margin:0;font-family: arial, sans-serif;width:100%;">
    <table width="auto" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
            <td align="right"><img src="https://colibris-lemouvement.org/sites/default/files/uploads/28/colibris.png"></td>
            <td>
                <div style="border-left:1px solid #aaa;padding:10px 25px 10px 25px;margin-left:25px;font-size:14px;color:#5a696b;line-height:20px;">
                      <ul style="font-weight:bold;font-size:16px;color:#333333;padding:0;margin:0;">'.$prenom.' '.$nom.'</ul>
                    <ul style="font-size:16px;color:#333333;padding:0;margin:0;margin-bottom:5px;">' .$role. '</ul>
                    <ul style="padding:0;margin:0;">Association Colibris <br>
                        55, rue des Vignoles - 75020 Paris<br><br>
                        ' .$tel. '</ul>
                    <ul style="padding:0;margin:0;"><a style="text-decoration:none;" href="http://www.colibris-lemouvement.org"><span style="font-size:14px;color:#b9cd00;font-weight:bold;">www.colibris-lemouvement.org</span></a></ul>
                </div>
            </td>
        </tr>
		'. $info_banner .'
      </tbody>
    </table>
	<br><br>
</div>

</body>
</html>';
				echo($signature_html);

				echo('<br><br><textarea rows="25" id="masignature" style="display:none;" class="form-control">');

				echo(htmlspecialchars($signature_html));

				echo('</textarea>');

			
			}


		?>

		Les champs indiqués par <span class="red">*</span> sont obligatoires.
		<br>

		<form method="POST">

			<div class="form-group">

				<label for="prenom">Prénom <span class="red"><span class="red">*</span></span></label>

				<input required type="text" class="form-control" id="prenom" name="prenom"
				       placeholder="Votre prénom..." value="<?php echo $_POST['prenom']; ?>">

			</div>

			<div class="form-group">

				<label for="nom">Nom <span class="red">*</span></label>

				<input required type="text" class="form-control" id="nom" name="nom" placeholder="Votre nom..." value="<?php echo $_POST['nom']; ?>">

			</div>

			<div class="form-group">

				<label for="fonction">Fonction <span class="red">*</span></label>

				<input required type="text" class="form-control" id="fonction" name="formrole" placeholder="Votre fonction..." value="<?php echo $_POST['formrole']; ?>">

			</div>

			<div class="form-group">

				<label for="email">Adresse mail <span class="red">*</span></label>

				<input required type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp"

				       placeholder="Votre email @colibris-lemouvement.org" value="<?php echo $_POST['email']; ?>">

			</div>

			<div class="form-group">

				<label for="tel-bureau">Téléphone Bureau</label>

				<input type="tel" class="form-control" id="tel-bureau" name="tel-bureau"

				       aria-describedby="Téléphone bureau"

				       placeholder="(laisser vide si vous n'êtes pas joignable au bureau)"
					   
					   value="<?php echo $_POST['tel-bureau']; ?>"  >

			</div>

			<div class="form-group">

				<label for="tel-mobile">Téléphone Mobile</label>

				<input type="tel" class="form-control" id="tel-mobile" name="tel-mobile"

				       aria-describedby="Téléphone mobile"

				       placeholder="0655555506"
					   
					   value="<?php echo $_POST['tel-mobile']; ?>"  >

			</div>

			<div class="form-group">

				<label for="bandeau">Bandeau du moment (supprimez le lien pour ne pas intégrer de bannière)</label>

				<input type="url" class="form-control" id="url_img_banner" name="url_img_banner"

	   				aria-describedby="Rajouter le lien vers l'image/bandeau du moment"

					placeholder="https://www.colibris-lemouvement.org/IMAGEBANDEAU"

	  				value="
					<?php
					if (empty($_POST["url_img_banner"]) == false) {

						echo $_POST["url_img_banner"];
	
					} else {
	
						echo "https://nuage.colibris-lemouvement.org/apps/files_sharing/publicpreview/atJHXb9jzrmbMqw?x=1360&y=249&a=true&file=bandeau_agenda_2022.png&scalingup=0";
	
					}
					?>">
			</div>

			<div class="form-group">

				<label for="bandeau">Lien cible du bandeau</label>

				<input type="url" class="form-control" id="url_target_banner" name="url_target_banner"

					aria-describedby="Lien cible du bandeau"

					placeholder="https://www.colibris-lemouvement.org/EVENEMENT"

					value="
					<?php
					if (empty($_POST["url_target_banner"]) == false) {

						echo $_POST["url_target_banner"];
	
					} else {
	
						echo "https://www.colibris-laboutique.org/accueil/669-agenda-colibris-2022-9782374252391.html";
	
					}
					?>">
			</div>

			

			<button type="submit" class="btn btn-primary" name="submit">C'est parti !</button>

		</form>


	</main>

</div>

</body>

<script src="jquery-3.3.1.slim.min.js"></script>

<script src="popper.min.js"></script>

<script src="bootstrap.min.js"></script>

</html>

